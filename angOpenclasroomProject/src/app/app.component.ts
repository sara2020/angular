import { Component,OnInit } from '@angular/core';
import { Post } from './post.service';
import { AppareilService } from './services/appareil.service';
import { Observable, Subscription } from 'rxjs';
import 'rxjs/add/observable/interval';
declare const myTest: any;
declare const my:any;
declare const bulboff:any;
declare const bulbon:any;




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
 
  secondes: number;
  counterSubscription: Subscription;
  constructor() {

  }
  

  ngOnInit() {
    const counter = Observable.interval(1000);
    this.counterSubscription = counter.subscribe(
      (value) => {
        this.secondes = value;
      },
      (error) => {
        console.log('Uh-oh, an error occurred! : ' + error);
      },
      () => {
        console.log('Observable complete!');
      }
    );
  }

 
  ngOnDestroy() {
    this.counterSubscription.unsubscribe();
  }
  
  onClick() {
    myTest();
  }
  onAlert(){
my();
  }
  onBulboff(){
    document.getElementById('myImage').setAttribute('src','./assets/js/bulboff.jpg');
  }
  onBulbon(){
    document.getElementById('myImage').setAttribute('src','./assets/js/bulbon.jpg');
  }
  

}