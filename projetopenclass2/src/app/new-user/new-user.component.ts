import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { User } from '../models/User.model';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss']
})
export class NewUserComponent implements OnInit {
userForm:FormGroup;
  constructor(private formBuilder:FormBuilder,private userService:UserService,private router:Router) { }

  ngOnInit() {
    this.initForm();
  }
  initForm(){
this.userForm=this.formBuilder.group({
  firstName:'',
  lastName:'',
  email:'',
  drinkPreference:'',
  hobbies:this.formBuilder.array([])
})
  }
  onSubmit(){
    const user=new User(  
this.userForm.value.firstName,
this.userForm.value['lastName'],
this.userForm.value['email'],
this.userForm.value['drinkPreference'],
this.userForm.value['hobbies']?this.userForm.value['hobbies']:[]
    );
    this.userService.addUser(user);
    this.userService.saveUsersToServer();
    this.router.navigate(['users']);

  }
  getHobbies():FormArray{
    return this.userForm.get('hobbies')as FormArray
  }
  onAddHobby() {
    const newHobbyControl = this.formBuilder.control(null, Validators.required);
    this.getHobbies().push(newHobbyControl);
}

}
