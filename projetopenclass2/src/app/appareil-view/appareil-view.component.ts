import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppareilService } from '../services/appareil.service';

@Component({
  selector: 'app-appareil-view',
  templateUrl: './appareil-view.component.html',
  styleUrls: ['./appareil-view.component.scss']
})
export class AppareilViewComponent implements OnInit ,OnDestroy{
  appareils:any[];
  title = 'projetopenclass2';
  isAuth=false;
  lastUpdate = new Promise((resolve, reject) => {
    const date = new Date();
    setTimeout(
      () => {
        resolve(date);
      }, 2000
    );
  });
  appCongelateur="congélateur";
  appTele="tele";
  appOrdinateur="ordinateur";
  constructor(private appareilService:AppareilService) {
    
    this.appareils=this.appareilService.appareils;
    console.table(this.appareils)
    

    setTimeout(
      () => {
        this.isAuth = true;
      }, 4000
    );
  }
    ngOnInit(){
   

   

      }
  ngOnDestroy(){
    this.appareilService.saveAppareilsToServer();
  }
  onAllumer(){
    this.appareilService.switchOnAll();
    const  sar="sara".charAt(0);
    console.log(sar)
   }
   onEteindre(){
    if(confirm('étes vous sur de vouloir éteindre tous les appareils?')){
 this.appareilService.switchOffAll();
    }else{
 return null;
    }
    
   }
   
   
}
