import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { AppareilService } from '../services/appareil.service';
// declare  var jQuery:  any;
// function hello() {
//   alert('Hello!!!');
// }



@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
authStatus:boolean;
  constructor(private authService:AuthService,private router:Router,private appareilService:AppareilService) { }

  ngOnInit() {
    this.authStatus=this.authService.isAuth;
    // hello();
    
    // (function ($) {
    //   $(document).ready(function(){
    //     console.log("Hello from jQuery!");
    //   });
    // })(jQuery);
  }
  
  
onSignIn(){
  this.authService.signIn().then(
()=>{
  console.log('sign in is succesful');
  this.authStatus=this.authService.isAuth;
this.router.navigate(['apareils'])
}
  )
}
onSignOut(){
  
  this.authService.signOut();
  this.authStatus=this.authService.isAuth;
}
}
