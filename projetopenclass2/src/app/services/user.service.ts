import { User } from '../models/User.model';
import { HttpClient } from '@angular/common/http';
import { Injectable, OnInit, OnDestroy } from '@angular/core';
@Injectable()

export class UserService implements OnInit{
    public users:User[]=[
        new User('Will', 'Alexander', 'will@will.com', 'jus d\'orange', ['coder', 'boire du café'])
    ];

constructor(private httpClient:HttpClient){
}
ngOnInit(){
  
}


saveUsersToServer() {
  this.httpClient
    .put('https://gestion-d-appareils.firebaseio.com/users.json', this.users)
    .subscribe(
      () => {
        console.log('Enregistrement terminé !');
      },
      (error) => {
        console.log('Erreur ! : ' + error);
      }
    );
}
getUsersFromServer() {

    this.httpClient
      .get<any[]>('https://gestion-d-appareils.firebaseio.com/users.json')
      .subscribe(
        (response) => {
          this.users = response;
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        },
        ()=>{
            console.log('les donnéés de users sont obtenue du server')
        }
      );
}
    addUser(user:User){
        
this.users.push(user);
console.log('est ajouté avec succes')
    }
}