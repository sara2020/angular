import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AppareilService implements OnInit {
  appareils = [{ appareilName: 'appareilName', appareilStatus: 'éteint', id: 1 }

  ]
  constructor(private httpClient: HttpClient) {
    this.saveAppareilsToServer();
  }
  ngOnInit() {

  }
  saveAppareilsToServer() {
    this.httpClient
      .put('https://gestion-d-appareils.firebaseio.com/appareils.json', this.appareils)
      .subscribe(
        () => {
          console.log('Enregistrement des appareils  terminé !');
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }
  getAppareilsFromServer() {
    this.httpClient
      .get<any[]>('https://gestion-d-appareils.firebaseio.com/appareils.json')
      .subscribe(
        (response) => {
          this.appareils = response;

        },
        (error) => {
          console.log('Erreur ! : ' + error);
        },
        () => { console.log('get esr ddddddddd') }
      );
  }
  switchOnAll() {
    for (let appareil of this.appareils) {
      appareil.appareilStatus = 'allumé'
    }
  }
  switchOffAll() {
    for (let appareil of this.appareils) {
      appareil.appareilStatus = 'éteint'

    }
  }
  switchOnOne(i: number) {
    this.appareils[i].appareilStatus = "allumé"
  }
  switchOffOne(i: number) {
    this.appareils[i].appareilStatus = "éteint"
  }

  getAppareilById(id: number) {
    const appareil = this.appareils.find(
      (s) => {
        return s.id === id
      }
    );

    return appareil
  }
  addAppareil(name: string, status: string) {
    const appareilObject = {
      id: 0,
      appareilName: '',
      appareilStatus: ''
    }
    appareilObject.appareilName = name;
    appareilObject.appareilStatus = status;
    appareilObject.id = this.appareils[this.appareils.length - 1].id + 1;
    console.log(this.appareils.length)
    this.appareils.push(appareilObject);


  }
  delete(i: number) {
    this.appareils.splice(i, 1);
  }
}