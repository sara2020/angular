import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppareilViewComponent } from './appareil-view/appareil-view.component';
import { AuthComponent } from './auth/auth.component';
import { SingleAppareilComponent } from './single-appareil/single-appareil.component';
import { FourOhFourComponent } from './four-oh-four/four-oh-four.component';
import { AuthGuard } from './services/auth-guard';
import { EditAppareilComponent } from './edit-appareil/edit-appareil.component';
import { UserListComponent } from './user-list/user-list.component';
import { NewUserComponent } from './new-user/new-user.component';

const routes: Routes = [
  {path:'apareils',component:AppareilViewComponent,canActivate:[AuthGuard]},
  {path:'auth',component:AuthComponent},
  {path:'edit',component:EditAppareilComponent,canActivate:[AuthGuard]},
  {path:'users',component:UserListComponent,canActivate:[AuthGuard]},
  {path:'newuser',component:NewUserComponent,canActivate:[AuthGuard]},

  {path:'',component:AppareilViewComponent},
  {path:'apareils/:id',component:SingleAppareilComponent,canActivate:[AuthGuard]},
  { path: 'not-found', component: FourOhFourComponent },
  { path: '**', redirectTo: 'not-found' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
