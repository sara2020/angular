import { Component, OnInit, OnDestroy } from '@angular/core';

import { AppareilService } from './services/appareil.service';
import { NullAstVisitor } from '@angular/compiler';
import { Observable, Subscription, from } from 'rxjs';
import 'rxjs/add/observable/interval';
import 'rxjs/add/observable/of';
import { UserService } from './services/user.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  secondes: number;
  counterSubscription: Subscription;
  constructor(private appareilService:AppareilService,private userService:UserService) {
  }
  ngOnInit() {
    this.appareilService.getAppareilsFromServer();
    this.userService.getUsersFromServer();
    const counter = Observable.interval(1000);
    const oo=Observable.of(42);
    const uu=from([1,2,0]);
    this.counterSubscription = counter.subscribe(
      (value) => { this.secondes = value },
      (error) => { console.log('il y a une erreur' + error) },
      () => { console.log('observable complete') }

    );
  }
  ngOnDestroy() {
    this.counterSubscription.unsubscribe();
  }

}
