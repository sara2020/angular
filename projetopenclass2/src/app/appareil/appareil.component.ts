import { Component, OnInit, Input } from '@angular/core';
import { AppareilService } from '../services/appareil.service';

@Component({
  selector: 'app-appareil',
  templateUrl: './appareil.component.html',
  styleUrls: ['./appareil.component.scss']
})
export class AppareilComponent implements OnInit {
@Input() appareilName="téléviseur";
@Input() appareilStatus:string;
@Input() index:number;
@Input() id:number;
  constructor(private appareilService:AppareilService) { }

  ngOnInit() {
  }
  getStatus(){
    return this.appareilStatus
  }
  getColor(){
    if(this.appareilStatus==='allumé'){
      return 'green'
    }else if(this.appareilStatus==='éteint')
    return 'red'
  }
  onAllumer(){
    this.appareilService.switchOnOne(this.index);

  }
  onEteindre(){
    if(confirm('es tu sur de vouloir éteindre cet appareil')){
this.appareilService.switchOffOne(this.index);
    }else{
return null;
    }
    

  }
  delette(){

    this.appareilService.delete(this.index);

    }
}
